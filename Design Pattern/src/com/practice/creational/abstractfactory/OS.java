package com.practice.creational.abstractfactory;

public interface OS {
     public void spec();
}
