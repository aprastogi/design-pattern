package com.practice.creational.abstractfactory;

public interface AbstractFactory {
     OS getInstance();
}
