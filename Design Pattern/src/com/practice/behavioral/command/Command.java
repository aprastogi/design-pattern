package com.practice.behavioral.command;

import java.util.List;

public class Command {
    private String name;
    private List<String> param;

    public String getName() {
        return name;
    }

    public List<String> getParam() {
        return param;
    }
}
